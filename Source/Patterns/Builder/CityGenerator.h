// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CanGenerateCity.h"
#include "UObject/NoExportTypes.h"
#include "CityGenerator.generated.h"

/**
 * Class to generate a elven city
 */
UCLASS(Blueprintable, BlueprintType)
class PATTERNS_API UElvenCityGenerator : public UObject, public ICanGenerateCity
{
	GENERATED_BODY()

public:
	/** BEGIN: ICanGenerateCity interface implementation */
	virtual UObject* GetResult() const override;
	virtual void GenerateGrounds() override;
	virtual void GenerateCastle() override;
	virtual void GenerateHourse() override;
	/** END: ICanGenerateCity interface implementation */

protected:
	void SpawnRoot(){};
	void SpawnGrass(){};
	void SpawnTrees(){};
	void SpawnRivers(){};
	void SpawnTreeCastle(){};
	void SpawnTreeHourse(){};

private:
	/** The root of the city. */
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "City", meta = (AllowPrivateAccess = "True"))
		AActor* CityRoot;
};

inline UObject* UElvenCityGenerator::GetResult() const
{
	return CityRoot;
}

inline void UElvenCityGenerator::GenerateGrounds()
{
	SpawnRoot();
	SpawnGrass();
	SpawnTrees();
	SpawnRivers();
}

inline void UElvenCityGenerator::GenerateCastle()
{
	SpawnTreeCastle();
}

inline void UElvenCityGenerator::GenerateHourse()
{
	SpawnTreeHourse();
}

/**
 * Class to generate a necromantic city
 */
UCLASS(Blueprintable, BlueprintType)
class UNecroCityGenerator : public UObject, public ICanGenerateCity
{
	GENERATED_BODY()
public:
	/** BEGIN: ICanGenerateCity interface implementation */
	virtual UObject* GetResult() const override;
	virtual void GenerateGrounds() override;
	virtual void GenerateHourse() override;
	/** END: ICanGenerateCity interface implementation */

protected:
	void SpawnRoot(){};
	void SpawnDeadGround(){};
	void SpawnStyx(){};
	void SpawnLeta(){};
	void SpawnCemetery(){};
	void SpawnDom(){};
	void SpawnTombs(){};

private:
	/** The root of the city */
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "City", meta = (AllowPrivateAccess = "True"))
		AActor* CityRoot;	
};

inline UObject* UNecroCityGenerator::GetResult() const
{
	return CityRoot;
}

inline void UNecroCityGenerator::GenerateGrounds()
{
	SpawnRoot();
	SpawnDeadGround();
	SpawnStyx();
	SpawnLeta();
}

inline void UNecroCityGenerator::GenerateHourse()
{
	SpawnCemetery();
	SpawnDom();
	SpawnTombs();
}

/**
 * Class to generate some basic quest structure for given area.
 */
UCLASS()
class UCityQuestGenerator : public UObject, public ICanGenerateCity
{
	GENERATED_BODY()
public:
	/** BEGIN: ICanGenerateCity interface implementation */
	virtual UObject* GetResult() const override { return QuestRoot; }
	virtual void GenerateCastle() override {};
	virtual void GenerateGrounds() override {};
	virtual void GenerateHourse() override {};
	/** END: ICanGenerateCity interface implementation */

private:
	UObject* QuestRoot;
};
