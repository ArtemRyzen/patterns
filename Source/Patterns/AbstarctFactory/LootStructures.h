// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "LootStructures.generated.h"

USTRUCT(BlueprintType)
struct FBaseInfo
{
	GENERATED_BODY()
	
};

USTRUCT(BlueprintType)
struct FArmorInfo : public FBaseInfo
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "LootStats")
		int64 Durability;
};

USTRUCT(BlueprintType)
struct FWeaponInfo : public FBaseInfo
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "LootStats")
		float Damage;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "LootStats")
		int64 Durability;
};